export default class ReceiptPrinter {
  constructor (products) {
    this.products = [...products];
  }

  print (barcodes) {
    // TODO: Please implement the method
    // <-start-
    let allSelectedProductsReceipt = [];
    let totalPrice = 0;

    if (barcodes.length === 0) {
      allSelectedProductsReceipt = '\n';
      totalPrice = 0;
      return this.formatReceipt(allSelectedProductsReceipt, totalPrice);
    }

    const allBarcodes = this.products.map(product => product.barcode);
    for (let i = 0; i < barcodes.length; i++) {
      const judgeBarcodes = allBarcodes.includes(barcodes[i]);
      if (judgeBarcodes === false) {
        throw new Error('Unknown barcode.');
      }
    }

    barcodes.sort();
    const deduplicatedBarcodes = Array.from(new Set(barcodes));

    for (let i = 0; i < deduplicatedBarcodes.length; i++) {
      const selectedProduct = this.products.find(product => product.barcode === deduplicatedBarcodes[i]);
      const productCount = barcodes.filter(barcode => barcode === deduplicatedBarcodes[i]).length;
      const selectedProductReceipt = `${selectedProduct.name}`.padEnd(30) + `x${productCount}        ${selectedProduct.unit}\n`;
      allSelectedProductsReceipt += selectedProductReceipt;
      totalPrice += selectedProduct.price * productCount;
    }

    return this.formatReceipt(allSelectedProductsReceipt, totalPrice);
  }

  formatReceipt (allSelectedProductsReceipt, totalPrice) {
    const formattedReceipt = '==================== Receipt ====================\n' +
                             `${allSelectedProductsReceipt}\n` +
                             '===================== Total =====================\n' +
                             `${totalPrice.toFixed(2)}`;

    return formattedReceipt;
  }

  // --end->
}
